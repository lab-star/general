<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

class RefreshCommand extends Command
{
    protected $signature = 'app:refresh';
    protected $description = 'Refresh DB and storage';

    public function handle(): int
    {
        if (app()->isProduction()) {
            return self::FAILURE;
        }

        $this->call('migrate:fresh', [
            '--seed' => true
        ]);

        Storage::deleteDirectory('images/products');

        return self::SUCCESS;
    }
}
