<?php

namespace App\Http\Controllers;

use App\Domains\Product\Models\Brand;
use App\Domains\Product\Models\Category;
use App\Domains\Product\Models\Product;
use App\Domains\User\Models\User;

class HomeController extends Controller
{
    public function __invoke()
    {
        return view('pages.home', [
            'users' => User::all(),
            'categories' => Category::all(),
            'products' => Product::all(),
            'brands' => Brand::all(),
        ]);
    }
}
