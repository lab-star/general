<?php

namespace App\Livewire\Supports;

use Illuminate\Contracts\View\View;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;

class IconChanger extends Component
{
    public ?string $icon = null;
    public bool $opened = false;

    protected array $rules = [
        'icon' => ['required', 'string']
    ];

    public function open(): void
    {
        $this->opened = true;
    }

    public function getIconsProperty(): Collection
    {
        return collect(Storage::disk('heroicons')->files())->map(function ($item) {
            return str($item)->remove('.php')->headline()->slug()->value();
        });
    }

    public function change(string $icon): void
    {
        $this->icon = $icon;
        $this->opened = false;

        $this->dispatch('icon-updated', $icon);
    }

    public function destroy(): void
    {
        $this->reset('icon');
        $this->dispatch('icon-updated');
    }

    public function render(): View
    {
        return view('livewire.supports.icon-changer');
    }
}
