<?php

namespace App\Livewire\Supports;

use Illuminate\Contracts\View\View;
use Livewire\Component;

class Alert extends Component
{
    public string $type = 'secondary';
    public ?string $message = null;
    public bool $show = false;

    public $listeners = [
        'makeAlert' => 'make'
    ];

    public function mount(): void
    {
        if(alert()->has()) {
            $this->make(
                type: alert()->get()->type(),
                message: alert()->get()->message()
            );
        }
    }

    public function make(string $type, string $message): void
    {
        $this->type = $type;
        $this->message = $message;
        $this->show = true;
        $this->dispatch('alert');
    }

    public function close(): void
    {
        $this->reset();
    }
    public function render(): View
    {
        return view('livewire.supports.alert');
    }
}
