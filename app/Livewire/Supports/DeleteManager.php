<?php

namespace App\Livewire\Supports;

use Illuminate\Contracts\View\View;
use Livewire\Attributes\On;
use Livewire\Component;
use App\Supports\Traits\Livewire\HasAlert;

class DeleteManager extends Component
{
    use HasAlert;

    public bool $modal = false;

    public ?string $model = null;
    public ?string $id = null;
    public ?string $redirect = null;

    #[On('delete-object')]
    public function open(string $subject, string $id, string $redirect): void
    {
        $this->model = str($subject)
            ->headline()
            ->replace(' ', '\\')
            ->value();
        $this->id = $id;
        $this->redirect = $redirect;
        $this->modal = true;
    }

    public function cancel(): void
    {
        $this->reset();
    }

    public function apply(): void
    {
        if (is_null($this->model) || is_null($this->id) || is_null($this->redirect)) {
            $this->alert(__('Ууупс... Кажется что-то пошло не так!'), 'error');
            return;
        }

        $this->model::find($this->id)->delete();

        alert()->primary(__('Успешно удалено!'));

        $this->redirect($this->redirect);
    }

    public function render(): View
    {
        return view('livewire.supports.delete-manager');
    }
}
