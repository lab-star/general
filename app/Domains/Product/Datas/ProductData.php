<?php

namespace App\Domains\Product\Datas;

use App\Supports\Enums\Currency;
use Spatie\LaravelData\Data;

final class ProductData extends Data
{
    public function __construct(
        public string $name,
        public ?string $brand_id = null,
        public ?string $slug = null,
        public ?string $image = null,
        public int $price = 0,
        public Currency $currency = Currency::UAH,
        public bool $on_home = false,
        public int $sorting = 999,
        public bool $hidden = false
    ) {}
}
