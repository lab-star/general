<?php

namespace App\Domains\Product\Datas;

use Spatie\LaravelData\Data;

final class CategoryData extends Data
{
    public function __construct(
        public string $name,
        public ?string $category_id = null,
        public ?string $slug = null,
        public ?string $image = null,
        public ?string $intro = null,
        public bool $on_home = false,
        public int $sorting = 999,
        public bool $hidden = false
    ) {}
}
