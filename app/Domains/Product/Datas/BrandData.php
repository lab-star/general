<?php

namespace App\Domains\Product\Datas;

use Spatie\LaravelData\Data;

final class BrandData extends Data
{
    public function __construct(
        public string $name,
        public ?string $slug = null,
        public ?string $logo = null,
        public ?string $url = null,
        public ?string $country = null,
        public ?string $intro = null,
        public bool $on_home = false,
        public int $sorting = 999,
        public bool $hidden = false
    ) {}
}
