<?php

namespace App\Domains\Product\Models;

use App\Supports\Traits\HasSlug;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use HasSlug;
    use HasUuids;
    use SoftDeletes;

    protected $fillable = [
        'category_id',
        'slug',
        'name',
        'image',
        'intro',
        'on_home',
        'sorting',
        'hidden',
    ];

    protected function casts(): array
    {
        return [
            'on_home' => 'boolean',
            'hidden' => 'boolean',
        ];
    }

    public function scopeOnHome(Builder $query): void
    {
        $query->whereTrue('on_home')
            ->orderBy('sorting')
            ->limit('6');
    }

    public function brand(): BelongsTo
    {
        return $this->belongsTo(Brand::class);
    }

    public function products(): BelongsToMany
    {
        return $this->belongsToMany(Product::class);
    }
}
