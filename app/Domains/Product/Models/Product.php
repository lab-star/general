<?php

namespace App\Domains\Product\Models;

use App\Supports\Casts\PriceCast;
use App\Supports\Enums\Currency;
use App\Supports\Traits\HasSlug;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use HasSlug;
    use HasUuids;
    use SoftDeletes;

    protected $fillable = [
        'brand_id',
        'slug',
        'name',
        'image',
        'price',
        'currency',
        'on_home',
        'sorting',
        'hidden',
    ];

    protected function casts(): array
    {
        return [
            'price' => PriceCast::class,
            'currency' => Currency::class,
            'on_home' => 'boolean',
            'hidden' => 'boolean',
        ];
    }

    public function scopeOnHome(Builder $query): void
    {
        $query->whereTrue('on_home')
            ->orderBy('sorting')
            ->limit('6');
    }

    public function categories(): BelongsToMany
    {
        return $this->belongsToMany(Category::class);
    }
}
