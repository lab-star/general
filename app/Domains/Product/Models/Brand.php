<?php

namespace App\Domains\Product\Models;

use App\Supports\Traits\HasSlug;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class Brand extends Model
{
    use HasSlug;
    use HasUuids;
    use SoftDeletes;

    protected $fillable = [
        'slug',
        'name',
        'logo',
        'url',
        'country',
        'intro',
        'on_home',
        'sorting',
        'hidden',
    ];

    protected function casts(): array
    {
        return [
            'on_home' => 'boolean',
            'hidden' => 'boolean',
        ];
    }

    public function scopeOnHome(Builder $query): void
    {
        $query->whereTrue('on_home')
            ->orderBy('sorting')
            ->limit('6');
    }

    public function products(): HasMany
    {
        return $this->hasMany(Product::class);
    }
}
