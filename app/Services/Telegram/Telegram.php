<?php

namespace App\Services\Telegram;

use Illuminate\Support\Facades\Log;
use App\Services\Telegram\Exceptions\TelegramException;

class Telegram
{
    private static bool $fake = false;
    protected string $chat_id;
    protected string $message;
    protected string $parse_mode;
    protected bool $disable_notification = false;

    public static function fake(): void
    {
        self::$fake = true;
    }

    public function fromView(string $view, array $params = []): static
    {
        $this->setMessage(view($view, $params));
        $this->setParseMode('HTML');
        return $this;
    }

    public function setChat(string $chat_id): static
    {
        $this->chat_id = $chat_id;
        return $this;
    }

    public function setMessage(string $message): static
    {
        $this->message = $message . PHP_EOL . 'Отправлено из ' . config('app.name');
        return $this;
    }

    public function setParseMode(string $mode): static
    {
        $this->parse_mode = $mode;
        return $this;
    }

    public function disableNotification(): static
    {
        $this->disable_notification = true;
        return $this;
    }

    public function send(): void
    {

        if (self::$fake) return;

        try {
            if (!isset($this->chat_id)) {
                throw new TelegramException('Не был указан чат при отправлении сообщения через телеграм.');
            }

            if (!isset($this->message)) {
                throw new TelegramException('Не было указано сообщение при отправлении сообщения через телеграм.');
            }

            TelegramBotApi::sendMessage(
                config('telegram-chat.admin.token'),
                $this->chat_id,
                $this->message,
                $this->parse_mode,
                $this->disable_notification
            );
        } catch (TelegramException $e) {
            Log::error($e->getMessage(), [
                'chat_id' => $this->chat_id,
                'message' => $this->message,
                'parse_mode' => $this->parse_mode,
                'disable_notification' => $this->disable_notification
            ]);
        }
    }
}
