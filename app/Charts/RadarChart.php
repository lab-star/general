<?php

namespace App\Charts;

use ConsoleTVs\Charts\Classes\Chartjs\Chart;
use Supports\Datas\ChartData;

class RadarChart extends Chart
{
    public function __construct(ChartData $data)
    {
        parent::__construct();

        $this->minimalist(true);
        $this->displayLegend(true);

        $this->options(['options' => [
            'scales' => [
                'angleLines' => [
                    'display' => false
                ]
            ]
        ]]);

        $this->labels($data->labels);


        foreach ($data->datasets as $dataset) {
            $options = is_null($dataset->options) ? [] : $dataset->options;

            $this->dataset(
                name: $dataset->name,
                type: "radar",
                data: $dataset->items
            )->options([
                'backgroundColor' => $dataset->color->value . '44',
                'borderColor' => $dataset->color,
                'pointBackgroundColor'      => $dataset->color,
                'pointBorderColor'          => '#ffffff',
                'pointHoverBackgroundColor' => '#ffffff',
                'pointHoverBorderColor'     => $dataset->color,
                'borderWidth'               => 2,
                'pointRadius'               => 3,
                ...$options
            ]);
        }
    }
}
