<?php

namespace App\Charts;

use ConsoleTVs\Charts\Classes\Chartjs\Chart;
use Supports\Datas\ChartData;

class DoughnutChart extends Chart
{
    public function __construct(ChartData $data)
    {
        parent::__construct();

        $this->minimalist(true);
        $this->displayLegend(true);

        $this->labels($data->labels);

        foreach ($data->datasets as $dataset) {
            $this->dataset(
                    name: $dataset->name,
                    type: "doughnut",
                    data: $dataset->items
                )
                ->color('rgba(0,0,0,0.1)')
                ->backgroundColor($dataset->color);
        }
    }
}
