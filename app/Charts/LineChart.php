<?php

namespace App\Charts;

use ConsoleTVs\Charts\Classes\Chartjs\Chart;
use Supports\Datas\ChartData;

class LineChart extends Chart
{
    public function __construct(ChartData $data)
    {
        parent::__construct();

//        $this->minimalist(true);
//        $this->displayLegend(true);

        $this->labels($data->labels);

        foreach ($data->datasets as $dataset) {
            $options = is_null($dataset->options) ? [] : $dataset->options;

            $this->dataset(
                name: $dataset->name,
                type: "line",
                data: $dataset->items
            )->options([
                'backgroundColor' => $dataset->color->value . '99',
                'borderColor' => $dataset->color,
                'pointBackgroundColor'      => $dataset->color,
                'pointBorderColor'          => '#ffffff',
                'pointHoverBackgroundColor' => '#ffffff',
                'pointHoverBorderColor'     => $dataset->color,
                'borderWidth'               => 1,
                'pointRadius'               => 3,
                ...$options
            ]);
        }
    }
}
