<?php

namespace App\Charts;

use ConsoleTVs\Charts\Classes\Chartjs\Chart;
use Supports\Datas\ChartData;

class BarChart extends Chart
{
    public function __construct(ChartData $data)
    {
        parent::__construct();

//        $this->minimalist(true);
//        $this->displayLegend(true);

        $this->labels($data->labels);

        foreach ($data->datasets as $dataset) {
            $options = is_null($dataset->options) ? [] : $dataset->options;

            $this->dataset(
                name: $dataset->name,
                type: "bar",
                data: $dataset->items
            )->options([
                'backgroundColor'           => collect($dataset->color)->map(fn ($item) => $item . '44')->all(),
                'hoverBackgroundColor'      => collect($dataset->color)->map(fn ($item) => $item . '66')->all(),
                'borderColor'               => collect($dataset->color)->map(fn ($item) => $item . 'aa')->all(),
                'hoverBorderColor'          => collect($dataset->color)->map(fn ($item) => $item . 'cc')->all(),
                'borderWidth'               => 1,
                ...$options
            ]);
        }
    }
}
