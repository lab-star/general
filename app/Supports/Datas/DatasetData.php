<?php

namespace App\Supports\Datas;

use Spatie\LaravelData\Data;
use App\Supports\Enums\ChartColor;

class DatasetData extends Data
{
    public function __construct(
        public string $name,
        public iterable $items,
        public ChartColor|iterable $color,
        public ?iterable $options = null,
    ) {}
}
