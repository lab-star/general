<?php

namespace App\Supports\Datas;

use Illuminate\Support\Collection;
use Spatie\LaravelData\Data;

class ChartData extends Data
{
    public function __construct(
        public Collection $labels,
        public Collection $datasets,
    ) {}
}
