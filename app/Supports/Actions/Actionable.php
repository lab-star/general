<?php

namespace App\Supports\Actions;

use App\Supports\Contracts\ActionableContract;

abstract class Actionable implements ActionableContract
{
    final public function __invoke(...$arguments)
    {
        return $this->handle(...$arguments);
    }
}
