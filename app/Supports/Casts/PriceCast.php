<?php

namespace App\Supports\Casts;

use App\Supports\Enums\Currency;
use Illuminate\Contracts\Database\Eloquent\CastsAttributes;
use App\Supports\ValueObjects\Price;

class PriceCast implements CastsAttributes
{
    public function get($model, string $key, $value, array $attributes): string
    {
        return (new Price(
            value: $value ?? 0,
            currency: $model->currency
        ))->formatted();
    }

    public function set($model, string $key, $value, array $attributes)
    {
        if (!$value instanceof Price) {
            $value = (new Price(
                value: $value ? $value * 100 : 0,
                currency: $model->currency ?? Currency::UAH
            ));
        }

        return $value->raw();
    }
}
