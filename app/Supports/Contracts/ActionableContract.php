<?php

namespace App\Supports\Contracts;

interface ActionableContract
{
    public function __invoke(...$arguments);

    public function handle(array $arguments);
}
