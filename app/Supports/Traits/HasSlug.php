<?php

namespace App\Supports\Traits;

use Illuminate\Database\Eloquent\Model;

trait HasSlug
{
    protected static function bootHasSlug(): void
    {
        static::creating(function (Model $item) {
            $item->slug = $item->slug ?? str($item->{self::slugFrom()})->append(now()->timestamp)->slug();
        });
    }

    public static function slugFrom(): string
    {
        return 'name';
    }
}
