<?php

namespace App\Supports\Traits\Livewire;

trait WithModal
{
    public bool $opened = false;

    private function open(): void
    {
        $this->opened = true;
    }

    private function close(): void
    {
        $this->opened = false;
    }
}
