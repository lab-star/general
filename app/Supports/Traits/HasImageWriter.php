<?php

namespace App\Supports\Traits;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Laravel\Facades\Image;

trait HasImageWriter
{
    /**
     * A function that displays a notification in the lower right corner of the screen.
     * @param UploadedFile $image uploaded image
     * @param string $dir uploaded file dir, default "other"
     * @param int|null $width width size
     * @param int|null $height height size
     * @param string|null $name file name
     * @return string
     */
    private function writeImage(
        UploadedFile $image,
        string $dir = 'other',
        ?int $width = null,
        ?int $height = null,
        ?string $name = null
    ): string
    {
        $storage = Storage::disk('images');

        if (!$storage->exists($dir)) {
            $storage->makeDirectory($dir);
        }

        $filename = is_null($name)
            ? str(str()->uuid())->append('.jpg')
            : str($name)->slug()->append('.jpg');

        if (is_null($width)) {
            Image::read($image)
                ->save($storage->path("$dir/$filename"));
        } else {
            Image::read($image)
                ->cover($width, $height)
                ->save($storage->path("$dir/$filename"));
        }

        return $filename;
    }
}
