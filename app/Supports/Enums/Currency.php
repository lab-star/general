<?php

namespace App\Supports\Enums;

enum Currency: string
{
    case UAH = 'uah';
    case USD = 'usd';
    case EUR = 'eur';
    case RUB = 'rub';
    case BTC = 'btc';
    case ETH = 'eth';

    public function getSymbol(): string
    {
        return match ($this) {
            Currency::UAH => '₴',
            Currency::USD => '$',
            Currency::EUR => '€',
            Currency::RUB => '₽',
            Currency::BTC => '₿',
            Currency::ETH => 'Ξ',
        };
    }

    public function getPrecision(): int
    {
        return match ($this) {
            Currency::UAH, Currency::EUR, Currency::USD, Currency::RUB => 100,
            Currency::BTC, Currency::ETH => 10000000,
        };
    }
}
