<?php

namespace App\Supports\Enums;

enum ChartColor: string
{
    case Red = '#ba514f';
    case Green = '#4fba51';
    case Yellow = '#b8ba4f';
    case Blue = '#4f83ba';
    case Purple = '#874fba';
    case Aqua = '#4fb8ba';
    case Orange = '#ba864f';
    case Gray = '#999999';
}
