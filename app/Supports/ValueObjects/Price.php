<?php

namespace App\Supports\ValueObjects;

use App\Supports\Enums\Currency;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Contracts\Support\Jsonable;
use Stringable;

readonly class Price implements Stringable, Jsonable, Arrayable
{
    public function __construct(
        private ?int $value,
        private Currency $currency = Currency::UAH,
    ) {}

    public function raw(): int
    {
        return $this->value;
    }

    public function value(): float|int
    {
        return $this->value / $this->currency->getPrecision();
    }

    public function currency(): Currency
    {
        return $this->currency;
    }

    public function symbol(): string
    {
        return $this->currency->getSymbol();
    }

    public function formatted(): string
    {
        return number_format(
            num: $this->value(),
            decimal_separator: ',',
            thousands_separator: ' '
        ) . ' ' . $this->symbol();
    }

    public function __toString(): string
    {
        return $this->value();
    }

    public function toJson($options = 0): string
    {
        return $this->value();
    }

    public function toArray(): string
    {
        return $this->value();
    }
}
