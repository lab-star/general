<?php

namespace Database\Factories;

use App\Domains\User\Datas\UserData;
use App\Domains\User\Models\User;
use Illuminate\Support\Str;

class UserFactory extends Factory
{
    public function definition(): array
    {
        return [
            'name' => fake()->name(),
            'email' => fake()->unique()->safeEmail(),
            'email_verified_at' => now(),
            'password' => bcrypt('1q2w3e4r'),
            'remember_token' => Str::random(10),
        ];
    }

    public function create(): User
    {
        return User::query()->create($this->data);
    }

    public function toDto(): UserData
    {
        return UserData::from($this->data);
    }
}
