<?php

namespace Database\Factories;

use App\Domains\Product\Datas\ProductData;
use App\Domains\Product\Models\Brand;
use App\Domains\Product\Models\Product;
use App\Supports\Enums\Currency;

class ProductFactory extends Factory
{
    public function definition(): array
    {
        $name = fake()->words(asText: true);

        return [
            'brand_id' => fake()->boolean(75)
                ? Brand::query()->inRandomOrder()->first()?->id
                : null,
            'name' => str($name)->ucfirst(),
            'slug' => str($name)->slug(),
            'image' => fake()->file(
                sourceDirectory: base_path('/resources/images/products'),
                targetDirectory: storage_path('app/public/images/products'),
                fullPath: false
            ),
            'price' => fake()->numberBetween(100, 10000),
            'currency' => fake()->randomElement(Currency::cases()),
            'on_home' => fake()->boolean(25),
            'sorting' => fake()->numberBetween(1, 999),
            'hidden' => false
        ];
    }

    public function create(): Product
    {
        return Product::query()->create($this->data);
    }

    public function toDto(): ProductData
    {
        return ProductData::from($this->data);
    }
}
