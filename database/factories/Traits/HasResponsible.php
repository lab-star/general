<?php

namespace Database\Factories\Traits;

use Domains\Users\Models\User;

trait HasResponsible
{
    public ?string $responsible = null;

    public function responsible(User $responsible): self
    {
        $clone = clone $this;
        $clone->responsible = $responsible->id;
        return $clone->extra();
    }
}
