<?php

namespace Database\Factories\Traits;

use Domains\Organizations\Models\Team;

trait HasTeam
{
    public ?string $team = null;

    public function team(Team $team): self
    {
        $clone = clone $this;
        $clone->team = $team->id;
        return $clone->extra();
    }
}
