<?php

namespace Database\Factories\Traits;

use Domains\Users\Models\User;

trait HasUser
{
    public ?string $user = null;

    public function user(User $user): self
    {
        $clone = clone $this;
        $clone->user = $user->id;
        return $clone->extra();
    }
}
