<?php

namespace Database\Factories\Traits;

use Domains\Organizations\Models\Organization;

trait HasOrganizatoin
{
    public ?string $organization = null;

    public function organization(Organization $organization): self
    {
        $clone = clone $this;
        $clone->organization = $organization->id;
        return $clone->extra();
    }
}
