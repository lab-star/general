<?php

namespace Database\Factories\Traits;

use Domains\Users\Models\User;

trait HasOwner
{
    public ?string $owner = null;

    public function owner(User $owner): self
    {
        $clone = clone $this;
        $clone->owner = $owner->id;
        return $clone->extra();
    }
}
