<?php

namespace Database\Factories;

use App\Domains\Product\Datas\BrandData;
use App\Domains\Product\Models\Brand;

class BrandFactory extends Factory
{
    public function definition(): array
    {
        $name = fake()->company;

        return [
            'name' => str($name)->ucfirst(),
            'slug' => str($name)->slug(),
            'logo' => fake()->file(
                sourceDirectory: base_path('/resources/images/products'),
                targetDirectory: storage_path('app/public/images/products'),
                fullPath: false
            ),
            'url' => fake()->url,
            'country' => fake()->country,
            'intro' => fake()->words(nb: 12, asText: true),
            'on_home' => fake()->boolean(25),
            'sorting' => fake()->numberBetween(1, 999),
            'hidden' => false
        ];
    }

    public function create(): Brand
    {
        return Brand::query()->create($this->data);
    }

    public function toDto(): BrandData
    {
        return BrandData::from($this->data);
    }
}
