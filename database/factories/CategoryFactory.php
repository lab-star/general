<?php

namespace Database\Factories;

use App\Domains\Product\Datas\CategoryData;
use App\Domains\Product\Models\Category;

class CategoryFactory extends Factory
{
    public function definition(): array
    {
        $name = fake()->words(asText: true);

        return [
            'category_id' => null,
            'name' => str($name)->ucfirst(),
            'slug' => str($name)->slug(),
            'image' => fake()->file(
                sourceDirectory: base_path('/resources/images/products'),
                targetDirectory: storage_path('app/public/images/products'),
                fullPath: false
            ),
            'intro' => fake()->words(nb: 12, asText: true),
            'on_home' => fake()->boolean(25),
            'sorting' => fake()->numberBetween(1, 999),
            'hidden' => false
        ];
    }

    public function create(): Category
    {
        return Category::query()->create($this->data);
    }

    public function toDto(): CategoryData
    {
        return CategoryData::from($this->data);
    }
}
