<?php

use App\Domains\Product\Models\Category;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up(): void
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->uuid('id')->primary();

            $table->foreignIdFor(Category::class)
                ->nullable()
                ->constrained()
                ->cascadeOnDelete();

            $table->string('slug')->unique();
            $table->string('name');
            $table->string('image')->nullable();
            $table->string('intro')->nullable();

            $table->boolean('on_home')->default(false);
            $table->integer('sorting')->default(999);
            $table->boolean('hidden')->default(false);

            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('categories');
    }
};
