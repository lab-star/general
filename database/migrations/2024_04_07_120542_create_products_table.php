<?php

use App\Domains\Product\Models\Brand;
use App\Domains\Product\Models\Category;
use App\Domains\Product\Models\Product;
use App\Supports\Enums\Currency;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up(): void
    {
        Schema::create('products', function (Blueprint $table) {
            $table->uuid('id')->primary();

            $table->foreignIdFor(Brand::class)
                ->nullable()
                ->constrained()
                ->nullOnDelete();

            $table->string('slug')->unique();
            $table->string('name');
            $table->string('image')->nullable();
            $table->integer('price')->default(0);
            $table->enum('currency', Arr::pluck(Currency::cases(), 'value'))->default(Currency::UAH->value);

            $table->boolean('on_home')->default(false);
            $table->integer('sorting')->default(999);
            $table->boolean('hidden')->default(false);

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('category_product', function (Blueprint $table) {
            $table->id();

            $table->foreignIdFor(Category::class)
                ->constrained()
                ->cascadeOnDelete();

            $table->foreignIdFor(Product::class)
                ->constrained()
                ->cascadeOnDelete();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('category_product');
        Schema::dropIfExists('products');
    }
};
