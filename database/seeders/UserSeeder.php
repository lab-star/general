<?php

namespace Database\Seeders;

use App\Supports\Traits\HasImageWriter;
use Database\Factories\UserFactory;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    use HasImageWriter;

    public static array $users = [
        [
            'name' => 'Jason Walker',
            'info' => 'Aggressive and unpredictable, Jason has the skills to be a serious threat in any race.',
        ],
        [
            'name' => 'Sofia Martinez',
            'info' => 'An ice cold professional racer, Sofia isn\'t easily lured into crashes and prides herself on racing clean.',
        ],
        [
            'name' => 'Sally Taylor',
            'info' => 'Cheerful and not too worried about results, Sally is usually happy go lucky, but she can turn mean if her car is hit too often.',
        ],
        [
            'name' => 'Frank Malcov',
            'info' => 'A once great driver from a previous era with too many crashes and collisions behind him. His behavior is erratic and age has dulled his skills.',
        ],
        [
            'name' => 'Katie Jackson',
            'info' => 'The former casual-drivers club president is now on the verge of a nervous breakdown after too many rush-hour traffic jams. You should give her plenty of track space!',
        ],
        [
            'name' => 'Ray Carter',
            'info' => 'Laid back driver with genuine racing talent, but sometimes seems indifferent about winning or losing. Skillful enough to win.. if he feels like it.',
        ],
    ];

    private string $domain = 'nanodepo.net';
    private string $password = '1q2w3e4r';

    public function run(): void
    {
        foreach (self::$users as $user) {
            UserFactory::new()->extra([
                'name' => $user['name'],
                'email' => str($user['name'])->slug('.')->append('@')->append($this->domain)->value(),
                'password' => bcrypt($this->password),
            ])->create();
        }
    }
}
