<?php

namespace Database\Seeders;

use Database\Factories\CategoryFactory;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    public function run(): void
    {
        $main = CategoryFactory::new()->create();

        CategoryFactory::new()->extra(['category_id' => $main->id])->times(3);

        CategoryFactory::new()->times(6);
    }
}
