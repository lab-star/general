<?php

use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;

Route::get('/', HomeController::class)->name('home');

Route::middleware(['auth:sanctum', 'web', 'verified'])->group(function () {
    Route::get('/dashboard', function () {
        return view('pages.home');
    })->name('dashboard');
});
