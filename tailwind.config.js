import defaultTheme from 'tailwindcss/defaultTheme';
import forms from '@tailwindcss/forms';
import typography from '@tailwindcss/typography';

export default {
    darkMode: 'class',

    content: [
        './vendor/laravel/framework/src/Illuminate/Pagination/resources/views/*.blade.php',
        './storage/framework/views/*.php',
        './resources/views/**/*.blade.php',
    ],

    safelist: [
        {
            pattern: /w-(24|28|32|36|40|44|48|52|56|60|64|72|80|88|96)/
        }
    ],

    theme: {
        backgroundSize: {
            24: '6rem',
            32: '8rem'
        },
        container: {
            center: true,
            padding: {
                DEFAULT: '0',
                sm: '1rem'
            },
        },
        extend: {
            fontFamily: {
                sans: ['Roboto', ...defaultTheme.fontFamily.sans]
            },
            spacing: {
                4.5: '18px',
                22: '5.5rem',
                88: '22rem',
                128: '32rem',
                144: '36rem'
            },
            minWidth: {
                '96': '24rem'
            },
            maxWidth: {
                '1/2': '50%',
                70: '17.5rem'
            },
            width: {
                13: '3.25rem',
                22: '5.5rem',
                88: '22rem',
                128: '32rem',
                144: '36rem',
                192: '48rem',
                200: '50rem',
                320: '80rem'
            },
            height: {
                22: '5.5rem',
                88: '22rem',
                128: '32rem',
                144: '36rem',
                192: '48rem',
                200: '50rem'
            },
            maxHeight: {
                'modal': '75vh',
            },
            opacity: {
                8: '0.08',
                11: '0.11',
                12: '0.12',
                14: '0.14',
                38: '0.38'
            },
            animation: {
                'loading': 'spin 1.6s linear infinite'
            },
            backdropBlur: {
                xs: '2px',
            }
        },
        colors: {
            transparent: 'transparent',
            current: 'currentColor',

            white: '#ffffff',
            black: '#000000',

            primary: {
                'DEFAULT': '#1261a4',
                100: '#ffffff',
                99: '#fdfcff',
                98: '#f8f9ff',
                95: '#eaf1ff',
                90: '#d2e4ff',
                80: '#a1c9ff',
                70: '#71aff7',
                60: '#5594db',
                50: '#377abf',
                40: '#0f61a4',
                35: '#005493',
                30: '#00487f',
                25: '#003d6c',
                20: '#00325a',
                10: '#001c37',
                0: '#000000'
            },

            secondary: {
                'DEFAULT': '#535f70',
                100: '#ffffff',
                99: '#fdfcff',
                98: '#f8f9ff',
                95: '#eaf1ff',
                90: '#d7e3f8',
                80: '#bbc7db',
                70: '#a0acbf',
                60: '#8692a4',
                50: '#6c788a',
                40: '#535f70',
                35: '#475364',
                30: '#3c4858',
                25: '#313c4c',
                20: '#253141',
                10: '#101c2b',
                0: '#000000'
            },

            tertiary: {
                'DEFAULT': '#6c5778',
                100: '#ffffff',
                99: '#fffbff',
                98: '#fff7fc',
                95: '#fcecff',
                90: '#f4d9ff',
                80: '#d8bde4',
                70: '#bba2c7',
                60: '#a088ac',
                50: '#856f91',
                40: '#6c5778',
                35: '#5f4b6b',
                30: '#533f5f',
                25: '#473453',
                20: '#3c2947',
                10: '#261431',
                0: '#000000'
            },

            error: {
                'DEFAULT': '#ba1a1a',
                100: '#ffffff',
                99: '#fffbff',
                98: '#fff8f7',
                95: '#ffedea',
                90: '#ffdad6',
                80: '#ffb4ab',
                70: '#ff897d',
                60: '#ff5449',
                50: '#de3730',
                40: '#ba1a1a',
                35: '#a80710',
                30: '#93000a',
                25: '#7e0007',
                20: '#690005',
                10: '#410002',
                0: '#000000'
            },

            neutral: {
                'DEFAULT': '#5d5e61',
                100: '#ffffff',
                99: '#fdfcff',
                98: '#faf9fd',
                95: '#f1f0f4',
                90: '#e3e2e6',
                80: '#c6c6ca',
                70: '#ababae',
                60: '#909094',
                50: '#76777a',
                40: '#5d5e61',
                35: '#515255',
                30: '#45474a',
                25: '#3a3b3e',
                20: '#2f3033',
                10: '#1a1c1e',
                0: '#000000'
            },

            'neutral-variant': {
                'DEFAULT': '#5a5f66',
                100: '#ffffff',
                99: '#fdfcff',
                98: '#f8f9ff',
                95: '#edf1fa',
                90: '#dfe2eb',
                80: '#c3c6cf',
                70: '#a7abb4',
                60: '#8d9199',
                50: '#73777f',
                40: '#5a5f66',
                35: '#4e535a',
                30: '#43474e',
                25: '#373c43',
                20: '#2c3137',
                10: '#181c22',
                0: '#000000'
            },

            light: {
                'primary': '#0f61a4',
                'on-primary': '#ffffff',
                'primary-container': '#d2e4ff',
                'on-primary-container': '#001c37',

                'secondary': '#535f70',
                'on-secondary': '#ffffff',
                'secondary-container': '#d7e3f8',
                'on-secondary-container': '#101c2b',

                'tertiary': '#6c5778',
                'on-tertiary': '#ffffff',
                'tertiary-container': '#f4d9ff',
                'on-tertiary-container': '#261431',

                'error': '#ba1a1a',
                'on-error': '#ffffff',
                'error-container': '#ffdad6',
                'on-error-container': '#410002',

                'background': '#fdfcff',
                'on-background': '#1a1c1e',

                'surface': '#fdfcff',
                'surface-variant': '#dfe2eb',
                'surface-container': '#eceeef',
                'on-surface': '#1a1c1e',
                'on-surface-variant': '#43474e',

                'inverse-primary': '#a1c9ff',
                'inverse-surface': '#2f3033',
                'inverse-on-surface': '#f1f0f4',

                'outline': '#73777f',
                'outline-variant': '#c3c6cf'
            },

            dark: {
                'primary': '#a1c9ff',
                'on-primary': '#00325a',
                'primary-container': '#00487f',
                'on-primary-container': '#d2e4ff',

                'secondary': '#bbc7db',
                'on-secondary': '#253141',
                'secondary-container': '#3c4858',
                'on-secondary-container': '#d7e3f8',

                'tertiary': '#d8bde4',
                'on-tertiary': '#3c2947',
                'tertiary-container': '#533f5f',
                'on-tertiary-container': '#f4d9ff',

                'error': '#ffb4ab',
                'on-error': '#690005',
                'error-container': '#93000a',
                'on-error-container': '#ffdad6',

                'background': '#1a1c1e',
                'on-background': '#e3e2e6',

                'surface': '#1a1c1e',
                'surface-variant': '#43474e',
                'surface-container': '#1d2021',
                'on-surface': '#e3e2e6',
                'on-surface-variant': '#c3c6cf',

                'inverse-primary': '#0f61a4',
                'inverse-surface': '#e3e2e6',
                'inverse-on-surface': '#1a1c1e',

                'outline': '#8d9199',
                'outline-variant': '#43474e'
            },
        }
    },

    plugins: [
        forms,
        typography
    ],
};
