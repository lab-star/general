<div>
    <input type="hidden" name="icon" value="{{ $icon }}">

    @if($icon)
        <x-supports.chip.input
            wire:click="destroy"
            :icon="$icon"
            :text="str($icon)->headline()->value()"
            active="true"
        />
    @else
        <x-supports.chip.dropdown wire:click="open" :text="__('Выбрать иконку')" />
    @endif

    <x-supports.modal.base wire:model="opened">
        <x-supports.title
            :title="__('Изменение иконок')"
            :subtitle="__('Иконки были взяты из набора Heroicons')"
        />

        <div class="flex flex-row flex-wrap items-center justify-between mt-3 -mx-1 text-light-on-surface dark:text-dark-on-surface">
            @foreach($this->icons as $i)
                <x-dynamic-component wire:click="change('{{ $i }}')" type="solid" :component="'icon::'.$i" class="w-10 h-10 m-1" />
            @endforeach
        </div>

        <div class="flex flex-col my-3 text-xs text-center text-light-outline-variant dark:text-dark-outline-variant">
            <div class="flex flex-row items-center justify-center">
                <div>With</div>
                <x-icon::heart type="micro" />
                <div>and coffee.</div>
            </div>
            <div>
                <a href="https://nanodepo.net" target="_blank">NanoDepo,</a>
                <a href="https://t.me/BernhardWilson" target="_blank">@BernhardWilson.</a>
            </div>
        </div>
    </x-supports.modal.base>
</div>
