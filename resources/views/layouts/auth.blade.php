<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,400;0,500;0,600;0,700;0,800;0,900;1,400;1,500&display=swap" rel="stylesheet" />

    <script>
        if (localStorage.getItem('theme') === '"dark"') {
            document.documentElement.classList.add('dark')
        } else {
            document.documentElement.classList.remove('dark')
        }
    </script>

    <!-- Alpine Plugins -->
    <script defer src="https://cdn.jsdelivr.net/npm/@alpinejs/anchor@3.x.x/dist/cdn.min.js"></script>

    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <!-- Styles -->
    @stack('styles')
    @livewireStyles
</head>
<body class="flex flex-col h-screen select-none text-light-on-surface dark:text-dark-on-surface bg-light-background dark:bg-dark-background font-sans antialiased overflow-y-auto overflow-x-hidden">

<main class="flex flex-col w-full sm:px-6 overflow-hidden">
    {{ $slot }}
</main>

@livewire('supports.alert')

@livewireScripts

<div x-ref="scrim" x-cloak class="hidden fixed inset-0 transform backdrop-blur-xs pointer-events-auto transition-all z-0">
    <div class="absolute inset-0 bg-light-surface-variant/50 dark:bg-dark-surface-variant/50"></div>
</div>

</body>
</html>
