<x-app-layout>

    <div class="flex flex-col sm:flex-row sm:flex-wrap -m-3 sm:py-6">
        <div class="flex flex-col flex-auto w-full md:w-1/2 lg:w-full xl:w-1/2 p-3">
            {{ $left }}
        </div>

        <div class="flex flex-col flex-auto w-full md:w-1/2 lg:w-full xl:w-1/2 p-3">
            {{ $right }}
        </div>
    </div>

</x-app-layout>
