<x-guest-layout>

    <div class="flex flex-row items-center justify-between p-3">
        <x-supports.chip.assist icon="bell" text="Make Alert" x-on:click="$dispatch('makeAlert', { type: 'primary', message: 'Hello world!' })" />
        <x-supports.switch-mode />
    </div>


    <div class="flex flex-col sm:flex-row sm:flex-wrap sm:py-6">
        <div class="flex flex-col flex-auto w-full md:w-1/2 lg:w-full xl:w-1/2 p-3">

            <x-supports.section.base title="Categories" class="mb-6">
                <x-supports.list>
                    @foreach($categories as $category)
                        <x-supports.list.item title="{{ $category->name }}" subtitle="{{ $category->slug }}" />
                    @endforeach
                </x-supports.list>
            </x-supports.section.base>

            <x-supports.section.base title="Products" class="mb-6">
                <x-supports.list>
                    @foreach($products as $product)
                        <x-supports.list.item title="{{ $product->name }}" subtitle="{{ $product->price }}" />
                    @endforeach
                </x-supports.list>
            </x-supports.section.base>

        </div>
        <div class="flex flex-col flex-auto w-full md:w-1/2 lg:w-full xl:w-1/2 p-3">

            <x-supports.section.base title="Brands" class="mb-6">
                <x-supports.list>
                    @foreach($brands as $brand)
                        <x-supports.list.item title="{{ $brand->name }}" subtitle="{{ $brand->slug }}" />
                    @endforeach
                </x-supports.list>
            </x-supports.section.base>

            <x-supports.section.base title="Users" class="mb-6">
                <x-supports.list>
                    @foreach($users as $user)
                        <x-supports.list.item title="{{ $user->name }}" subtitle="{{ $user->email }}" />
                    @endforeach
                </x-supports.list>
            </x-supports.section.base>

        </div>
    </div>

</x-guest-layout>
