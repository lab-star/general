<x-app-layout>

    <x-slot name="header">
        <x-navigation.top.bar>
            <x-slot name="left">
                <x-navigation.top.icon icon="bell" badge badge_count="5" />

                <x-navigation.top.icon icon="calendar" badge />
            </x-slot>

            <div class="w-full">
                <x-supports.input.text name="search" label="Search input" />
            </div>

            <x-slot name="right">
                <x-navigation.top.icon icon="user-circle" />
            </x-slot>
        </x-navigation.top.bar>
    </x-slot>

    <div class="flex flex-col justify-center items-center w-full mb-6">

        <div class="my-6 text-2xl font-medium">Buttons</div>

        <div x-data="{ show: false }" class="flex flex-row justify-center flex-wrap mb-3">
            <x-supports.button.filled icon="plus" class="m-1">Primary</x-supports.button.filled>
            <x-supports.button.filled color="secondary" x-on:click="show = !show" icon="check" interactive="show" class="m-1">Secondary</x-supports.button.filled>
            <x-supports.button.filled color="tertiary" icon="cog-8-tooth" animation="animate-loading" class="m-1">Tertiary</x-supports.button.filled>
            <x-supports.button.filled color="error" class="m-1">Error</x-supports.button.filled>
            <x-supports.button.filled :disabled="true" class="m-1">Disabled</x-supports.button.filled>
        </div>

        <div class="flex flex-row justify-center flex-wrap mb-3">
            <x-supports.button.tonal icon="plus" class="m-1">Primary</x-supports.button.tonal>
            <x-supports.button.tonal x-data="{ show: false }" color="secondary" x-on:click="show = !show" icon="check" interactive="show" class="m-1">Secondary</x-supports.button.tonal>
            <x-supports.button.tonal color="tertiary" icon="cog-8-tooth" animation="animate-loading" class="m-1">Tertiary</x-supports.button.tonal>
            <x-supports.button.tonal color="error" class="m-1">Error</x-supports.button.tonal>
            <x-supports.button.tonal :disabled="true" class="m-1">Disabled</x-supports.button.tonal>
        </div>

        <div x-data="{ show: false }" class="flex flex-row justify-center flex-wrap mb-3">
            <x-supports.button.elevated icon="plus" class="m-1">Primary</x-supports.button.elevated>
            <x-supports.button.elevated color="secondary" x-on:click="show = !show" icon="check" interactive="show" class="m-1">Secondary</x-supports.button.elevated>
            <x-supports.button.elevated color="tertiary" icon="cog-8-tooth" animation="animate-loading" class="m-1">Tertiary</x-supports.button.elevated>
            <x-supports.button.elevated color="error" class="m-1">Error</x-supports.button.elevated>
            <x-supports.button.elevated :disabled="true" class="m-1">Disabled</x-supports.button.elevated>
        </div>

        <div x-data="{ show: false }" class="flex flex-row justify-center flex-wrap mb-3">
            <x-supports.button.outlined icon="plus" class="m-1">Primary</x-supports.button.outlined>
            <x-supports.button.outlined color="secondary" x-on:click="show = !show" icon="check" interactive="show" class="m-1">Secondary</x-supports.button.outlined>
            <x-supports.button.outlined color="tertiary" icon="cog-8-tooth" animation="animate-loading" class="m-1">Tertiary</x-supports.button.outlined>
            <x-supports.button.outlined color="error" class="m-1">Error</x-supports.button.outlined>
            <x-supports.button.outlined :disabled="true" class="m-1">Disabled</x-supports.button.outlined>
        </div>

        <div x-data="{ show: false }" class="flex flex-row justify-center flex-wrap mb-3">
            <x-supports.button.text icon="plus" class="m-1">Primary</x-supports.button.text>
            <x-supports.button.text color="secondary" x-on:click="show = !show" icon="check" interactive="show" class="m-1">Secondary</x-supports.button.text>
            <x-supports.button.text color="tertiary" icon="cog-8-tooth" animation="animate-loading" class="m-1">Tertiary</x-supports.button.text>
            <x-supports.button.text color="error" class="m-1">Error</x-supports.button.text>
            <x-supports.button.text :disabled="true" class="m-1">Disabled</x-supports.button.text>
        </div>



        <div class="my-6 text-2xl font-medium">Filled Inputs</div>

        <div class="flex flex-row justify-center flex-wrap w-full mb-3">
            <div class="w-64 p-3">
                <x-supports.input.filled.text
                    type="text"
                    name="name1"
                    label="Label"
                />
            </div>
            <div class="w-64 p-3">
                <x-supports.input.filled.text
                    type="text"
                    name="name2"
                    label="Filled"
                    value="Abracadabra!"
                    hint="Lorem ipsum dolor"
                    max="20"
                />
            </div>
            <div class="w-64 p-3">
                <x-supports.input.filled.text
                    type="text"
                    name="name3"
                    label="Required"
                    required
                    max="20"
                />
            </div>
            <div class="w-64 p-3">
                <x-supports.input.filled.text
                    type="text"
                    name="name4"
                    label="Disabled"
                    disabled
                />
            </div>
        </div>


        <div class="my-6 text-2xl font-medium">Outline Inputs</div>

        <div class="flex flex-row justify-center flex-wrap w-full mb-3">
            <div class="w-64 p-3">
                <x-supports.input.outlined.text
                    type="text"
                    name="name1"
                    label="Label"
                />
            </div>
            <div class="w-64 p-3">
                <x-supports.input.outlined.text
                    type="text"
                    name="name2"
                    label="Filled"
                    value="Abracadabra!"
                    hint="Lorem ipsum dolor"
                    max="20"
                />
            </div>
            <div class="w-64 p-3">
                <x-supports.input.outlined.text
                    type="text"
                    name="name3"
                    label="Required"
                    required
                    max="20"
                />
            </div>
            <div class="w-64 p-3">
                <x-supports.input.outlined.text
                    type="text"
                    name="name4"
                    label="Disabled"
                    disabled
                />
            </div>
        </div>

        <div class="my-6 text-2xl font-medium">Color Picker Inputs</div>

        <div class="flex flex-row justify-center flex-wrap w-full mb-3">
            <div class="w-96 p-3">
                <x-supports.input.outlined.color-picker
                    name="color1"
                    label="Color"
                    value="#ff5252"
                />
            </div>
            <div class="w-96 p-3">
                <x-supports.input.filled.color-picker
                    name="color2"
                    label="Color"
                    value="#0088cc"
                />
            </div>
        </div>


        <div class="my-6 text-2xl font-medium">Textarea</div>

        <div class="flex flex-row justify-center flex-wrap w-full mb-3">
            <div class="w-96 p-3">
                <x-supports.input.filled.textarea
                    name="textarea1"
                    label="Filled"
                />
            </div>
        </div>


        <div class="my-6 text-2xl font-medium">Select</div>

        <div class="flex flex-row justify-center flex-wrap w-full mb-3">
            <div class="w-96 p-3">
                <x-supports.input.filled.select
                    name="select1"
                    label="Filled Selecct"
                >
                    <x-supports.input.filled.select-item>First Item</x-supports.input.filled.select-item>
                    <x-supports.input.filled.select-item>Second Item</x-supports.input.filled.select-item>
                    <x-supports.input.filled.select-item>Last Item</x-supports.input.filled.select-item>
                </x-supports.input.filled.select>
            </div>
        </div>

        <div class="my-6 text-2xl font-medium">File Inputs</div>

        <div class="flex flex-row justify-center flex-wrap w-full mb-3">
            <div class="w-32 h-32">
                <x-supports.input.file
                    name="file"
                    label="Photo"
                />
            </div>
        </div>

        <div class="my-6 text-2xl font-medium">Other Inputs</div>

        <div class="flex flex-row justify-center flex-wrap w-full mb-3">
            <div class="w-64 p-3">
                <x-supports.input.checkbox
                    name="checkbox"
                    label="Checkbox"
                />
            </div>
            <div class="w-64 p-3">
                <x-supports.input.checkbox
                    name="checkbox"
                    label="Checked"
                    checked
                />
            </div>
            <div class="w-64 p-3">
                <x-supports.input.checkbox
                    name="checkbox"
                    label="Hinting label"
                    hint="Lorem ipsum dolor"
                />
            </div>
            <div class="w-64 p-3">
                <x-supports.input.checkbox
                    name="checkbox"
                    label="Disabled"
                    disabled
                />
            </div>
        </div>

        <div class="flex flex-row justify-center flex-wrap w-full mt-3 mb-3">
            <div class="w-32 p-3">
                <x-supports.input.switch
                    name="switch"
                />
            </div>
            <div class="w-32 p-3">
                <x-supports.input.switch
                    name="switch"
                    checked
                />
            </div>
            <div class="w-32 p-3">
                <x-supports.input.switch
                    name="switch"
                    disabled
                />
            </div>
            <div class="w-32 p-3">
                <x-supports.input.switch
                    name="switch"
                    checked
                    disabled
                />
            </div>
        </div>


        <div class="my-6 text-2xl font-medium">Chips</div>

        <div class="flex flex-row items-center flex-wrap mb-3">
            <x-supports.chip.assist class="m-1" text="Assist" />
            <x-supports.chip.assist :active="true" class="m-1" text="Active" />
            <x-supports.chip.assist icon="user" class="m-1" text="Icon" />
            <x-supports.chip.assist icon="cog-8-tooth" class="m-1" />
            <x-supports.chip.assist action="click" class="m-1" text="Action" />
            <x-supports.chip.assist class="m-1" :disabled="true" text="Disabled" />
            <x-supports.chip.assist color="secondary" icon="adjustments-vertical" class="m-1" text="Secondary" />
            <x-supports.chip.assist color="tertiary" icon="cloud-arrow-down" class="m-1" text="Tertiary" />
            <x-supports.chip.assist color="error" icon="trash" class="m-1" text="Error" />
        </div>

        <div class="flex flex-row items-center flex-wrap mb-3">
            <x-supports.chip.input class="m-1" text="Input" />
            <x-supports.chip.input :active="true" class="m-1" text="Active" />
            <x-supports.chip.input icon="user" class="m-1" text="Icon" />
            <x-supports.chip.input icon="cog-8-tooth" class="m-1" />
            <x-supports.chip.input action="click" class="m-1" text="Action" />
            <x-supports.chip.input class="m-1" :disabled="true" text="Disabled" />
            <x-supports.chip.input color="secondary" icon="adjustments-vertical" class="m-1" text="Secondary" />
            <x-supports.chip.input color="tertiary" icon="cloud-arrow-down" class="m-1" text="Tertiary" />
            <x-supports.chip.input color="error" icon="trash" class="m-1" text="Error" />
        </div>

        <div class="flex flex-row items-center flex-wrap mb-3">
            <x-supports.chip.dropdown class="m-1" text="Dropdown" />
            <x-supports.chip.dropdown :active="true" class="m-1" text="Active" />
            <x-supports.chip.dropdown icon="user" class="m-1" text="Icon" />
            <x-supports.chip.dropdown icon="cog-8-tooth" class="m-1" />
            <x-supports.chip.dropdown action="click" class="m-1" text="Action" />
            <x-supports.chip.dropdown class="m-1" :disabled="true" text="Disabled" />
            <x-supports.chip.dropdown color="secondary" icon="adjustments-vertical" class="m-1" text="Secondary" />
            <x-supports.chip.dropdown color="tertiary" icon="cloud-arrow-down" class="m-1" text="Tertiary" />
            <x-supports.chip.dropdown color="error" icon="trash" class="m-1" text="Error" />
        </div>



        <div class="my-6 text-2xl font-medium">Dropdowns</div>

        <div class="flex flex-row items-center flex-wrap mb-3">

            <x-supports.dropdown align="left">
                <x-slot name="trigger">
                    <x-supports.chip.dropdown class="m-1" icon="calendar" text="Выберите период" />
                </x-slot>
                <x-slot name="content">
                    <x-supports.dropdown.item>За год</x-supports.dropdown.item>
                    <x-supports.dropdown.item>За месяц</x-supports.dropdown.item>
                    <x-supports.dropdown.item>За неделю</x-supports.dropdown.item>
                </x-slot>
            </x-supports.dropdown>

            <x-supports.dropdown align="right">
                <x-slot name="trigger">
                    <x-supports.chip.dropdown class="m-1" text="Dropdown Bottom Right" />
                </x-slot>
                <x-slot name="content">
                    <x-supports.dropdown.item icon="heart">За год abracadabra 222 lorem</x-supports.dropdown.item>
                    <x-supports.dropdown.item disabled>За месяц</x-supports.dropdown.item>
                    <x-supports.dropdown.item>За неделю abracadabra 222 lorem</x-supports.dropdown.item>
                </x-slot>
            </x-supports.dropdown>

            <x-supports.dropdown align="bottom" dropdown-classes="bottom-8">
                <x-slot name="trigger">
                    <x-supports.chip.dropdown class="m-1" text="Dropdown Top" />
                </x-slot>
                <x-slot name="content">
                    <div class="p-4">
                        <x-supports.input.filled.text name="name" label="Name" />
                    </div>
                </x-slot>
            </x-supports.dropdown>

        </div>

        <div class="my-6 text-2xl font-medium">Sections & Title</div>

        <div class="flex flex-row justify-center flex-wrap w-full mb-3">
            <div class="w-96 p-3">
                <x-supports.section.base>
                    Empty section
                </x-supports.section.base>
            </div>

            <div class="w-96 p-3">
                <x-supports.section.base title="Lorem ipsum!">
                    Section where title != null
                </x-supports.section.base>
            </div>

            <div class="w-96 p-3">
                <x-supports.section.base title="Lorem ipsum!" subtitle="Dolor sit amet, consectetur...">
                    Section where title != null and where subtitle != null
                </x-supports.section.base>
            </div>
        </div>

        <div class="flex flex-row justify-center flex-wrap w-full mb-3">
            <div class="w-96 p-3">
                <x-supports.section.base title="Lorem ipsum!" subtitle="Dolor sit amet, consectetur...">
                    <x-slot name="action">
                        <x-supports.button.filled>Edit</x-supports.button.filled>
                    </x-slot>

                    Section where title != null and where subtitle != null and where isset action
                </x-supports.section.base>
            </div>

            <div class="w-96 p-3">
                <x-supports.section.base>
                    Empty section where isset footer

                    <x-slot name="footer">
                        <x-supports.button.filled color="tertiary">Submit</x-supports.button.filled>
                    </x-slot>
                </x-supports.section.base>
            </div>

            <div class="w-96 p-3">
                <x-supports.section.base title="Lorem ipsum!" subtitle="Dolor sit amet, consectetur...">
                    <x-slot name="action">
                        <x-supports.button.filled>Edit</x-supports.button.filled>
                    </x-slot>

                    Full props section

                    <x-slot name="footer">
                        <x-supports.button.text color="secondary">Cancel</x-supports.button.text>
                        <x-supports.button.filled color="tertiary">Submit</x-supports.button.filled>
                    </x-slot>
                </x-supports.section.base>
            </div>
        </div>

        <div class="flex flex-row justify-center flex-wrap w-full mb-3">
            <div class="w-full sm:w-1/2 p-3">
                <x-supports.section.base
                    title="Двоичный список"
                    subtitle="Секция со списком из двух элементов, предназначина для примитивных данных"
                >
                    <div class="-mx-6">
                        <x-supports.section.list-item key="First item" value="Description First item" />
                        <x-supports.section.list-item key="Second item" value="Description Second item" />
                        <x-supports.section.list-item key="Last item" value="Description Last item" />
                    </div>
                </x-supports.section.base>
            </div>

            <div class="w-full sm:w-1/2 p-3">
                <x-supports.section.base
                    title="Троичный список"
                    subtitle="Секция со списком из трех элементов, предназначина для примитивных данных с необязательной третьей колонкой"
                >
                    <div class="-mx-6">
                        <x-supports.section.list-item key="First item" value="Description First item">
                            $200
                        </x-supports.section.list-item>
                        <x-supports.section.list-item key="Second item" value="Description Second item">
                            <x-supports.input.switch name="section-switch" />
                        </x-supports.section.list-item>
                        <x-supports.section.list-item key="Last item" value="Description Last item">
                            <x-supports.chip.assist icon="cog-8-tooth" text="Settings" />
                        </x-supports.section.list-item>
                    </div>

                    <x-slot name="footer">
                        <x-supports.button.text color="error" icon="trash">Delete</x-supports.button.text>
                        <x-supports.button.filled icon="fire">Deploy Now</x-supports.button.filled>
                    </x-slot>
                </x-supports.section.base>
            </div>
        </div>


        <div  class="flex flex-col justify-center items-center w-full mb-6">

            <div class="my-6 text-2xl font-medium">Modal</div>

            <div class="flex flex-row justify-center flex-wrap mb-3">
                <div x-data="{ modal: false }">
                    <x-supports.button.outlined x-on:click="modal = true" icon="bolt" class="m-1">Modal</x-supports.button.outlined>

                    <x-supports.modal.base>
                        <p class="mb-3">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad asperiores aut debitis ea illo incidunt ipsum iusto provident quae quibusdam quos ratione sequi, sint vero voluptatibus. Animi debitis ducimus facilis.</p>
                        <p class="">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad asperiores aut debitis ea illo incidunt ipsum iusto provident quae quibusdam quos ratione sequi, sint vero voluptatibus. Animi debitis ducimus facilis.</p>

                        <x-slot name="footer">
                            <x-supports.button.text>Cancel</x-supports.button.text>
                            <x-supports.button.filled>Create</x-supports.button.filled>
                        </x-slot>
                    </x-supports.modal.base>
                </div>

                <div x-data="{ modal: false }">
                    <x-supports.button.outlined x-on:click="modal = true" icon="bolt" class="m-1">Confirmation</x-supports.button.outlined>

                    <x-supports.modal.confirmation title="If you're sure?" subtitle="It will be impossible to recover the data.">
                        <x-slot name="footer">
                            <x-supports.button.text x-on:click="modal = false">Cancel</x-supports.button.text>
                            <x-supports.button.filled color="error">Drop my life</x-supports.button.filled>
                        </x-slot>
                    </x-supports.modal.confirmation>
                </div>
            </div>

        </div>



        <div class="draggable--over">asdasd</div>



    </div>

</x-app-layout>
