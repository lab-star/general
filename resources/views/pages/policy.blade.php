<x-guest-layout width="sm:max-w-2xl">

    <x-supports.section.base class="relative my-6 overflow-hidden">

        <div class="prose">
            {!! $content !!}

            <img src="{{ asset('images/cat.webp') }}" alt="" class="absolute bottom-0 right-4 -mb-6 w-32 h-32 m-0 p-0">
        </div>

    </x-supports.section.base>

</x-guest-layout>
