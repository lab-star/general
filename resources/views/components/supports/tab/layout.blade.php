<div {{ $attributes->merge([
    'class' => 'flex flex-row justify-between flex-auto items-end border-b border-light-outline-variant dark:border-dark-outline-variant text-sm text-light-on-surface-variant dark:text-dark-on-surface-variant overflow-x-auto overflow-y-hidden'
]) }}>
    {{ $slot }}
</div>
