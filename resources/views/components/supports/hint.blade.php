@props([
    'align' => 'left',
    'width' => 44,
])

<div
    x-data="{ hint: false, x: 0, y: 0 }"
    x-on:mouseenter="hint = true; x = $event.x; y = $event.y;"
    x-on:mouseleave="hint = false"
    class="flex flex-col relative"
>
    {{ $slot }}

    <div
        x-show="hint"
        x-data="{ w: {{ $align == 'left' ? str($width)->toInteger() * 4 : 0 }} }"
        x-bind:style="{ position: 'fixed', top: y+'px', left: (x - w)+'px' }"
        x-transition:enter="transition ease-out duration-200"
        x-transition:enter-start="transform opacity-0 scale-95"
        x-transition:enter-end="transform opacity-100 scale-100"
        x-transition:leave="transition ease-in duration-75"
        x-transition:leave-start="transform opacity-100 scale-100"
        x-transition:leave-end="transform opacity-0 scale-95"
        {{ $attributes->merge(['class' => 'px-4 py-2 text-xs bg-white dark:bg-black text-light-on-surface dark:text-dark-on-surface rounded cursor-help z-10'])->class('w-'.$width) }}
        style="display: none"
    >
        {{ $hint }}
    </div>
</div>
