<div
    x-data="{
        theme: $persist('light').as('theme'),
        toggle() {
            if (this.theme == 'dark') {
                this.theme = 'light';
                document.documentElement.classList.remove('dark')
            } else {
                this.theme = 'dark';
                document.documentElement.classList.add('dark')
            }
        }
    }"
    x-on:click="toggle()"
    class="flex flex-col"
>
    <x-supports.chip.assist x-show="theme == 'dark'" icon="sun" text="Light" />
    <x-supports.chip.assist x-show="theme == 'light'" icon="moon" text="Dark" />
</div>
