@props([
    'title' => null,
    'completed' => false,
    'active' => false,
    'first' => false,
    'last' => false,
])

@if(!$first)
    <div @class([
        'flex-auto h-1',
        'bg-light-primary dark:bg-dark-primary' => $completed || $active,
        'bg-light-outline-variant dark:bg-dark-outline-variant' => !$completed && !$active,
    ])></div>
@endif

@if($completed)
    <div class="flex-none w-7 h-7 p-1 bg-light-primary dark:bg-dark-primary rounded-full" title="{{ $title }}">
        <x-icon::check type="solid" class="w-5 h-5" />
    </div>
@elseif($active)
    <div class="flex-none w-5 h-5 p-1.5 bg-light-primary dark:bg-dark-primary rounded-full" title="{{ $title }}">
        <div class="w-2 h-2 bg-light-outline-variant dark:bg-dark-outline-variant rounded-full"></div>
    </div>
@else
    <div class="flex-none w-3 h-3 bg-light-primary dark:bg-dark-primary rounded-full" title="{{ $title }}"></div>
@endif

@if(!$last)
    <div @class([
        'flex-auto h-1',
        'bg-light-primary dark:bg-dark-primary' => $completed,
        'bg-light-outline-variant dark:bg-dark-outline-variant' => !$completed,
    ])></div>
@endif
