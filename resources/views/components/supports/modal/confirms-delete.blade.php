<x-supports.modal.confirmation
    wire:model="modal"
    :title="$title"
    :subtitle="$subtitle"
>
    <x-slot name="footer">
        <x-supports.button.text icon="trash" color="error" wire:click="apply">
            {{ __('Удалить') }}
        </x-supports.button.text>

        <x-supports.button.filled icon="face-smile" wire:click="cancel">
            {{ __('Нет, вернуться') }}
        </x-supports.button.filled>
    </x-slot>
</x-supports.modal.confirmation>
