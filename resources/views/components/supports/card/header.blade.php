<div class="flex flex-row flex-none items-center justify-between px-6 pt-6">
    {{ $slot }}
</div>
