<div {{ $attributes->merge(['class' => 'flex flex-col -mx-6 divide-y divide-light-primary/5 dark:divide-dark-primary/5']) }}>
    {{ $slot }}
</div>
