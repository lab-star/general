@props(['product'])

<x-supports.list.item
    {{ $attributes }}
    title="{{ $product->title }}"
>
    <x-slot name="object">
        <div class="relative">
            <x-supports.avatar class="w-9 h-9" :url="thumbnail($product->image?->path, '64x64', 'products')" />
        </div>
    </x-slot>

    <x-slot name="action">
        {{ $slot }}
    </x-slot>
</x-supports.list.item>
