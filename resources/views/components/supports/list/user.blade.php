@props(['user', 'href' => route('users.show', $user->id)])

<x-supports.list.item
    {{ $attributes }}
    href="{{ $href }}"
    title="{{ $user->contact->username }} {{ $user->contact->surname }}"
    :active="$user->id == auth()->id()"
>
    <x-slot name="object">
        <div class="relative">
            <x-supports.avatar class="w-9 h-9" :is_online="$user->is_online" :url="thumbnail($user->avatar, '64x64', 'profile')" />
        </div>
    </x-slot>

    <x-slot name="subtitle">
        <div class="inline-flex flex-row items-center space-x-2">
            @if($user->teammate?->role)
                <x-supports.meta icon="identification" :text="$user->teammate?->role" title="{{ $user->teammate?->description }}" />
            @endif
            <x-supports.meta icon="user-group" :text="str($user->teams->pluck('name')->join(', '))->limit(30)" title="{{ __('Команды в которых состоит пользователь') }}" />
        </div>
    </x-slot>

    <x-slot name="action">
        {{ $slot }}
    </x-slot>
</x-supports.list.item>
