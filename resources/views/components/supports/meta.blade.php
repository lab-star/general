@props(['icon', 'text'])

<div {{ $attributes->merge(['class' => 'flex flex-row items-center flex-none space-x-1 text-xs tracking-wide']) }}>
    <x-dynamic-component component="icon::{{ $icon }}" type="micro" />
    <div>{{ $text }}</div>
</div>
