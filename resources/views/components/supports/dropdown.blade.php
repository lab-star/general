@props([
    'align' => 'left',
    'width' => 44,
    'disabled' => false,
    'teleport' => 'body'
])

<div
    class="relative {{ $disabled ? 'pointer-events-none cursor-default opacity-50' : '' }}"
    x-data="{ open: false, x: 0, y: 0 }"
    x-modelable="open"
    {{ $attributes }}
>
    <div x-on:click="open = !open; x = $event.pageX ; y = $event.pageY">
        {{ $trigger }}
    </div>

    <template x-teleport="{{ $teleport }}">
        <div x-show="open"
             x-data="{ w: {{ $align == 'left' ? str($width)->toInteger() * 4 : 0 }} }"
             x-transition:enter="transition ease-out duration-200"
             x-transition:enter-start="transform opacity-0 scale-95"
             x-transition:enter-end="transform opacity-100 scale-100"
             x-transition:leave="transition ease-in duration-75"
             x-transition:leave-start="transform opacity-100 scale-100"
             x-transition:leave-end="transform opacity-0 scale-95"
             class="absolute z-50 mt-1 w-{{ $width }} rounded-md shadow-lg"
             style="display: none"
             x-bind:style="{ top: y+'px', left: (x - w) +'px' }"
             x-on:click.away="open = false"
             x-on:close.stop="open = false"
        >
            <div class="py-1 bg-white dark:bg-black  text-light-on-surface dark:text-dark-on-surface rounded-md">
                {{ $content }}
            </div>
        </div>
    </template>
</div>
