@props([
    'type' => 'button',
    'icon' => null,
    'disabled' => false,
    'animation' => null
])

@php
    $attr = $attributes->class([
        'relative inline-flex flex-row justify-start items-center py-2.5 font-medium leading-4 rounded-full text-sm tracking-wide focus:outline-none focus:ring-none transition cursor-pointer',
        'pr-6 pl-4' => $slot->isNotEmpty(),
        'px-2.5' => $slot->isEmpty(),
    ]);
@endphp

@if($type == 'link')
    <a {{ $attr }}>
        @if($icon)
            <div class="flex flex-none w-5 h-5">
                <x-dynamic-component
                    component="icon::{{ $icon }}"
                    type="mini"
                    class="{{ $animation }}"
                />
            </div>
        @endif
        @if($slot->isNotEmpty())
            <div class="flex flex-auto pl-2">{{ $slot }}</div>
        @endif
    </a>
@else
    <button {{ $attr }} type="{{ $type }}" @disabled($disabled) wire:loading.attr="disabled">
        @if($icon)
            <div class="flex flex-none w-5 h-5">
                <x-dynamic-component
                    component="icon::{{ $icon }}"
                    type="mini"
                    class="w-5 h-5 {{ $animation }}"
                />
            </div>
        @endif
        @if($slot->isNotEmpty())
            <div class="flex flex-auto pl-2">{{ $slot }}</div>
        @endif
    </button>
@endif
