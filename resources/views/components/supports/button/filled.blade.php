@props([
    'color' => 'primary',
    'disabled' => false,
])

<x-supports.button.layout
    {{ $attributes->class([
        'bg-light-primary text-light-on-primary dark:bg-dark-primary dark:text-dark-on-primary hover:shadow-light-1' => $color == 'primary' && !$disabled,
        'bg-light-secondary text-light-on-secondary dark:bg-dark-secondary dark:text-dark-on-secondary hover:shadow-light-1' => $color == 'secondary' && !$disabled,
        'bg-light-tertiary text-light-on-tertiary dark:bg-dark-tertiary dark:text-dark-on-tertiary hover:shadow-light-1' => $color == 'tertiary' && !$disabled,
        'bg-light-error text-light-on-error dark:bg-dark-error dark:text-dark-on-error hover:shadow-light-1' => $color == 'error' && !$disabled,
        'bg-light-on-surface/12 text-light-on-surface/38 dark:bg-dark-on-surface/12 dark:text-dark-on-surface/38 pointer-events-none' => $disabled,
    ]) }}
    :disabled="$disabled"
>
    {{ $slot }}
</x-supports.button.layout>
