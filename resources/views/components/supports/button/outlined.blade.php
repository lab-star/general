@props([
    'color' => 'primary',
    'disabled' => false,
])

<x-supports.button.layout
    {{ $attributes->class([
        'border border-light-outline dark:border-dark-outline focus:border-light-primary dark:focus:border-dark-primary hover:bg-light-primary/8 dark:hover:bg-dark-primary/8 focus:bg-light-primary/12 dark:focus:bg-dark-primary/12 active:bg-light-primary/12 dark:active:bg-dark-primary/12 text-light-on-primary-container dark:text-dark-on-primary-container' => $color == 'primary' && !$disabled,
        'border border-light-outline dark:border-dark-outline focus:border-light-secondary dark:focus:border-dark-secondary hover:bg-light-secondary/8 dark:hover:bg-dark-secondary/8 focus:bg-light-secondary/12 dark:focus:bg-dark-secondary/12 active:bg-light-secondary/12 dark:active:bg-dark-secondary/12 text-light-on-secondary-container dark:text-dark-on-secondary-container' => $color == 'secondary' && !$disabled,
        'border border-light-outline dark:border-dark-outline focus:border-light-tertiary dark:focus:border-dark-tertiary hover:bg-light-tertiary/8 dark:hover:bg-dark-tertiary/8 focus:bg-light-tertiary/12 dark:focus:bg-dark-tertiary/12 active:bg-light-tertiary/12 dark:active:bg-dark-tertiary/12 text-light-on-tertiary-container dark:text-dark-on-tertiary-container' => $color == 'tertiary' && !$disabled,
        'border border-light-outline dark:border-dark-outline focus:border-light-error dark:focus:border-dark-error hover:bg-light-error/8 dark:hover:bg-dark-error/8 focus:bg-light-error/12 dark:focus:bg-dark-error/12 active:bg-light-error/12 dark:active:bg-dark-error/12 text-light-on-error-container dark:text-dark-on-error-container' => $color == 'error' && !$disabled,
        'border border-light-on-surface/12 text-light-on-surface/38 dark:border-dark-on-surface/12 dark:text-dark-on-surface/38 pointer-events-none' => $disabled,
    ]) }}
    :disabled="$disabled"
>
    {{ $slot }}
</x-supports.button.layout>
