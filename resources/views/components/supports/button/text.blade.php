@props([
    'color' => 'primary',
    'disabled' => false,
])

<x-supports.button.layout
    {{ $attributes->class([
        'hover:bg-light-primary-container dark:hover:bg-dark-primary-container focus:bg-light-primary-container dark:focus:bg-dark-primary-container active:bg-light-primary-container dark:active:bg-dark-primary-container text-light-on-primary-container dark:text-dark-on-primary-container' => $color == 'primary' && !$disabled,
        'hover:bg-light-secondary-container dark:hover:bg-dark-secondary-container focus:bg-light-secondary-container dark:focus:bg-dark-secondary-container active:bg-light-secondary-container dark:active:bg-dark-secondary-container text-light-on-secondary-container dark:text-dark-on-secondary-container' => $color == 'secondary' && !$disabled,
        'hover:bg-light-tertiary-container dark:hover:bg-dark-tertiary-container focus:bg-light-tertiary-container dark:focus:bg-dark-tertiary-container active:bg-light-tertiary-container dark:active:bg-dark-tertiary-container text-light-on-tertiary-container dark:text-dark-on-tertiary-container' => $color == 'tertiary' && !$disabled,
        'hover:bg-light-error-container dark:hover:bg-dark-error-container focus:bg-light-error-container dark:focus:bg-dark-error-container active:bg-light-error-container dark:active:bg-dark-error-container text-light-on-error-container dark:text-dark-on-error-container' => $color == 'error' && !$disabled,
        'text-light-on-surface/38 dark:text-dark-on-surface/38 pointer-events-none' => $disabled,
    ]) }}
    :disabled="$disabled"
>
    {{ $slot }}
</x-supports.button.layout>
