@props([
    'color' => 'primary',
    'disabled' => false,
])

<x-supports.button.layout
    {{ $attributes->class([
        'bg-light-primary-container text-light-on-primary-container dark:bg-dark-primary-container dark:text-dark-on-primary-container hover:shadow-light-1' => $color == 'primary' && !$disabled,
        'bg-light-secondary-container text-light-on-secondary-container dark:bg-dark-secondary-container dark:text-dark-on-secondary-container hover:shadow-light-1' => $color == 'secondary' && !$disabled,
        'bg-light-tertiary-container text-light-on-tertiary-container dark:bg-dark-tertiary-container dark:text-dark-on-tertiary-container hover:shadow-light-1' => $color == 'tertiary' && !$disabled,
        'bg-light-error-container text-light-on-error-container dark:bg-dark-error-container dark:text-dark-on-error-container hover:shadow-light-1' => $color == 'error' && !$disabled,
        'bg-light-on-surface/12 text-light-on-surface/38 dark:bg-dark-on-surface/12 dark:text-dark-on-surface/38 pointer-events-none' => $disabled,
    ]) }}
    :disabled="$disabled"
>
    {{ $slot }}
</x-supports.button.layout>
