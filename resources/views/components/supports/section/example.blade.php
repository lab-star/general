<x-supports.section.base>

    <x-slot name="header">
        <x-navigation.top.bar title="Lorem ipsum">
            <x-slot name="left">
                <x-navigation.top.icon href="{{ route('home') }}" icon="arrow-left" />
            </x-slot>
            <x-slot name="right">
                <x-navigation.top.icon href="{{ route('teams.create') }}" icon="plus" />
            </x-slot>
        </x-navigation.top.bar>
    </x-slot>

    <x-slot name="title">
        Lotem ipsum
    </x-slot>

    <x-slot name="subtitle">
        Dolor sit amet, consectetur
    </x-slot>

    <x-slot name="action">
        <x-supports.input.switch name="status" />
    </x-slot>

    <x-slot name="content">
        <h1>Hashing</h1>
        <p class="mt-3">Bcrypt is a great choice for hashing passwords because its "work factor" is adjustable, which means that the time it takes to generate a hash can be increased as hardware power increases.</p>
    </x-slot>

    <x-slot name="footer">
        <x-supports.button.text icon="cog">Settings</x-supports.button.text>
        <x-supports.button.filled icon="heart">Apply</x-supports.button.filled>
    </x-slot>

</x-supports.section.base>
