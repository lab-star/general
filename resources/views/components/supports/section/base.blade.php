@props([
    'title' => null,
    'subtitle' => null,
    'action' => null,
    'disabled' => false,
    'loading' => false,
])

<div {{ $attributes->merge(['class' => 'relative flex flex-col flex-none w-full text-light-on-surface dark:text-dark-on-surface bg-light-primary/5 dark:bg-dark-primary/5 overflow-hidden sm:rounded-2xl'])->class(['opacity-50 pointer-events-none' => $disabled]) }}>
    <div
        class="flex flex-col w-full flex-auto overflow-hidden"
        @if($loading)
            wire:loading.class.delay.long="pointer-events-none"
        @endif
    >
        @if(isset($header))
            <div class="flex flex-col">
                {{ $header }}
            </div>
        @endif

        @if(!is_null($title))
            <div class="flex flex-col p-6">
                <x-supports.title :title="$title" :subtitle="$subtitle" :action="$action" />
            </div>
        @endif

        @if($slot->isNotEmpty() || isset($content))
            <div @class([
            'flex-auto overflow-hidden',
            'px-6 pb-6' => isset($title),
            'p-6' => !isset($title),
        ])>
                {{ $content ?? $slot }}
            </div>
        @endif

        @if(isset($footer))
            <div class="flex flex-row justify-between p-6 pt-3">
                {{ $footer }}
            </div>
        @endif
    </div>

    @if($loading)
        <div wire:loading.delay.long class="absolute top-0 right-0 bottom-0 left-0 inset-0 bg-light-surface-variant/25 dark:bg-dark-surface-variant/25">
            <div class="flex flex-col items-center justify-center flex-auto h-full">
                <div class="la-pacman dark:la-dark la-sm">
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                </div>
            </div>
        </div>
    @endif
</div>
