@props([
    'name' => '',
    'min' => 0,
    'max' => 100,
    'value' => null,
    'disabled' => false,
])

<div
    x-data="{ count: {{ $value ?? 0 }} }"
    x-modelable="count"
    {{ $attributes }}
    @class([
        'flex flex-row flex-none',
        'pointer-events-none opacity-50' => $disabled
    ])
>
    <div x-on:click="count--" x-bind:class="{ 'pointer-events-none opacity-50': count <= {{ $min }} }" class="flex items-center justify-center w-12 h-12 border border-light-outline dark:border-dark-outline rounded-l-lg cursor-pointer">
        <x-icon::minus class="w-5 h-5" />
    </div>
    <div class="flex items-center justify-center w-12 h-12 border-y border-light-outline dark:border-dark-outline" x-text="count"></div>
    <div x-on:click="count++" x-bind:class="{ 'pointer-events-none opacity-50': count >= {{ $max }} }" class="flex items-center justify-center w-12 h-12 border border-light-outline dark:border-dark-outline rounded-r-lg cursor-pointer">
        <x-icon::plus class="w-5 h-5" />
    </div>
</div>

@error(str($name)->replace('[', '.')->rtrim(']')->toString())
    <div class="py-2 text-sm font-medium text-light-error dark:text-dark-error">{{ $message }}</div>
@enderror
