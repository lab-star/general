@props(['active' => false])

<a {{ $attributes->merge(['class' => 'p-1 text-xs tracking-wide'])->class([
    'text-light-on-surface-variant dark:text-dark-on-surface-variant hover:text-light-primary dark:hover:text-dark-primary hover:underline transition' => !$active,
    'font-medium' => $active
]) }}>
    {{ $slot }}
</a>
