<div
    x-data="{ owner: @js(request()->ow ?? '') }"
    x-init="$watch('owner', () => $refs.filters.submit())"
    class="w-full sm:w-44 p-1"
>
    <x-supports.input.filled.select x-model="owner" :label="__('Автор')" name="ow">
        <x-supports.input.filled.select-item value="">{{ __('Не выбран') }}</x-supports.input.filled.select-item>
        @foreach($users as $owner)
            <x-supports.input.filled.select-item :value="$owner->id">{{ $owner->name }}</x-supports.input.filled.select-item>
        @endforeach
    </x-supports.input.filled.select>
</div>
