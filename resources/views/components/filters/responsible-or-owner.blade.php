<div
    x-data="{ ro: @js(request()->ro ?? '') }"
    x-init="$watch('ro', () => $refs.filters.submit())"
    class="w-full sm:w-44 p-1"
>
    <x-supports.input.filled.select x-model="ro" :label="__('Где сотрудник')" name="ro">
        <x-supports.input.filled.select-item value="">{{ __('Автор или ответственный') }}</x-supports.input.filled.select-item>
        <x-supports.input.filled.select-item value="owner">{{ __('Автор') }}</x-supports.input.filled.select-item>
        <x-supports.input.filled.select-item value="responsible">{{ __('Ответственный') }}</x-supports.input.filled.select-item>
    </x-supports.input.filled.select>
</div>
