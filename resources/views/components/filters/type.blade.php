<div
    x-data="{ type: @js(request()->tp ?? '') }"
    x-init="$watch('type', () => $refs.filters.submit())"
    class="w-full sm:w-32 p-1"
>
    <x-supports.input.filled.select x-model="type" :label="__('Тип задачи')" name="tp">
        <x-supports.input.filled.select-item value="">{{ __('Не выбран') }}</x-supports.input.filled.select-item>
        @foreach(\Domains\Gems\Sapphires\Enums\TaskType::cases() as $type)
            <x-supports.input.filled.select-item :value="$type->value">{{ __($type->name) }}</x-supports.input.filled.select-item>
        @endforeach
    </x-supports.input.filled.select>
</div>
