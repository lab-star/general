<div
    x-data="{ responsible: @js(request()->rs ?? '') }"
    x-init="$watch('responsible', () => $refs.filters.submit())"
    class="w-full sm:w-44 p-1"
>
    <x-supports.input.filled.select x-model="responsible" :label="__('Ответственный')" name="rs">
        <x-supports.input.filled.select-item value="">{{ __('Не выбран') }}</x-supports.input.filled.select-item>
        @foreach($users as $responsible)
            <x-supports.input.filled.select-item :value="$responsible->id">{{ $responsible->name }}</x-supports.input.filled.select-item>
        @endforeach
    </x-supports.input.filled.select>
</div>
