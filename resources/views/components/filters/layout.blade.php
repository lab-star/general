@props(['route' => route('boards')])

<form x-ref="filters" class="flex flex-col sm:flex-row flex-wrap items-end sm:items-center mb-6 p-3 sm:p-0 sm:-mx-1">

    {{ $slot }}

{{--    <div class="p-1">--}}
{{--        <x-supports.button.filled type="submit">{{ __('Применить') }}</x-supports.button.filled>--}}
{{--    </div>--}}

    <div class="p-1">
        <x-supports.button.tonal type="link" :href="$route" color="secondary">{{ __('Сбросить') }}</x-supports.button.tonal>
    </div>

</form>
