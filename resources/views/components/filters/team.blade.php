<div
    x-data="{ team: @js(request()->tm ?? '') }"
    x-init="$watch('team', () => $refs.filters.submit())"
    class="w-full sm:w-44 p-1"
>
    <x-supports.input.filled.select x-model="team" :label="__('Команда')" name="tm">
        <x-supports.input.filled.select-item value="">{{ __('Не выбрана') }}</x-supports.input.filled.select-item>
        @foreach($teams as $team)
            <x-supports.input.filled.select-item :value="$team->id">{{ $team->name }}</x-supports.input.filled.select-item>
        @endforeach
    </x-supports.input.filled.select>
</div>
