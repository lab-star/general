<div class="w-full sm:w-36 p-1">
    <x-supports.input.filled.text
        type="date"
        :label="__('Дата до')"
        name="to"
        :value="request()->to"
        disabled
    />
</div>
