<x-guest-layout>
    <div class="mb-6">
        <a href="https://loyalgift.ru">
            <img src="{{ asset('images/lg-10.png') }}" alt="" class="w-36">
        </a>
    </div>

    <x-supports.section.base>

        <form id="form-data" method="POST" action="{{ route('password.store') }}">
            @csrf

            <input type="hidden" name="token" value="{{ $request->route('token') }}">

            <div class="mb-6">
                <x-supports.input.filled.text
                    type="email"
                    name="email"
                    label="{{ __('Электронная почта') }}"
                    :value="old('email', $request->email)"
                    required
                    autofocus
                />
            </div>

            <div class="mb-6">
                <x-supports.input.filled.text
                    type="password"
                    name="password"
                    label="{{ __('Новый пароль') }}"
                    required
                    autofocus
                    autocomplete="new-password"
                />
            </div>

            <div class="">
                <x-supports.input.filled.text
                    type="password"
                    name="password_confirmation"
                    label="{{ __('Повторите новый пароль') }}"
                    required
                    autofocus
                    autocomplete="new-password"
                />
            </div>
        </form>

        <x-slot name="footer">
            <div></div>
            <x-supports.button.filled
                form="form-data"
                icon="lock-closed"
                type="submit"
            >
                {{ __('Сбросить') }}
            </x-supports.button.filled>
        </x-slot>
    </x-supports.section.base>
</x-guest-layout>
