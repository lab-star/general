<x-guest-layout>

    <div class="mb-6">
        <a href="https://loyalgift.ru">
            <img src="{{ asset('images/lg-10.png') }}" alt="" class="w-36">
        </a>
    </div>

    <x-supports.section.base>
        <x-slot name="title">{{ __('Подтверждение пароля') }}</x-slot>
        <x-slot name="subtitle">{{ __('Это безопасная область приложения. Пожалуйста, подтвердите свой пароль, прежде чем продолжить.') }}</x-slot>

        <form id="form-data" method="POST" action="{{ route('password.confirm') }}">
            @csrf

            <div>
                <x-supports.input.filled.text
                    name="password"
                    label="{{ __('Пароль') }}"
                    type="password"
                    required
                    autofocus
                    autocomplete="current-password"
                />
            </div>

        </form>

        <x-slot name="footer">
            <div></div>
            <x-supports.button.filled
                form="form-data"
                icon="finger-print"
                type="submit"
            >
                {{ __('Подтвердить') }}
            </x-supports.button.filled>
        </x-slot>
    </x-supports.section.base>
</x-guest-layout>
