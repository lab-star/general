<x-guest-layout>

    <div class="mb-6">
        <div>
            <img src="{{ asset('images/lg-10.png') }}" alt="" class="w-36">
        </div>
    </div>

    <x-supports.section.base>

        <div class="flex flex-row mb-4">
            <a href="{{ route('login') }}" class="flex flex-row flex-auto justify-center px-6 py-2 sm:py-4 border-b-4 border-light-primary dark:border-dark-primary">
                <x-icon::finger-print class="w-5 h-5" />
                <div class="ml-3 text-sm font-bold">{{ __('Войти') }}</div>
            </a>

            <a href="{{ route('register') }}" class="flex flex-row flex-auto justify-center px-6 py-2 sm:py-4 border-b-4 border-light-outline-variant hover:border-light-outline-variant dark:border-dark-outline-variant dark:hover:border-dark-outline-variant cursor-pointer transition">
                <x-icon::heart class="w-5 h-5" />
                <div class="ml-3 text-sm font-bold">{{ __('Регистрация') }}</div>
            </a>
        </div>

        <form id="form-data" method="POST" action="{{ route('login') }}">
            @csrf

            <div class="mt-6">
                <x-supports.input.filled.text
                    type="email"
                    name="email"
                    label="{{ __('Электронная почта') }}"
                    :value="old('email')"
                    required
                    autofocus
                />
            </div>

            <div class="mt-6">
                <x-supports.input.filled.text
                    type="password"
                    name="password"
                    label="{{ __('Пароль') }}"
                    required
                    autocomplete="current-password"
                />
            </div>

            <div class="mt-6">
                <x-supports.input.checkbox
                    name="remember"
                    label="{{ __('Запомнить меня') }}"
                />
            </div>
        </form>

        <x-slot name="footer">
            <a class="underline text-sm text-light-on-surface-variant hover:text-light-on-surface dark:text-dark-on-surface-variant dark:hover:text-dark-on-surface" href="{{ route('password.request') }}">
                {{ __('Забыли пароль?') }}
            </a>

            <x-supports.button.filled
                form="form-data"
                icon="finger-print"
                type="submit"
            >
                {{ __('Войти') }}
            </x-supports.button.filled>
        </x-slot>

    </x-supports.section.base>

</x-guest-layout>
