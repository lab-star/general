<x-guest-layout>
    <div class="mb-6">
        <a href="https://loyalgift.ru">
            <img src="{{ asset('images/lg-10.png') }}" alt="" class="w-36">
        </a>
    </div>

    <x-supports.section.base>
        <x-slot name="title">{{ __('Подтверждение электронной почты') }}</x-slot>
        <x-slot name="subtitle">{{ __('Прежде чем продолжить, не могли бы вы подтвердить свой адрес электронной почты, перейдя по ссылке, которую мы только что отправили вам по электронной почте? Если вы не получили электронное письмо, мы с радостью вышлем вам другое.') }}</x-slot>

        @if (session('status') == 'verification-link-sent')
            <div class="mb-4 font-medium text-sm text-light-primary">
                {{ __('На адрес электронной почты, который вы указали в настройках своего профиля, была отправлена новая ссылка для подтверждения.') }}
            </div>
        @endif

        <div class="mt-4 flex items-center justify-between">
            <form method="POST" action="{{ route('verification.send') }}">
                @csrf

                <div>
                    <x-supports.button.text type="submit">
                        {{ __('Выслать повторно письмо') }}
                    </x-supports.button.text>
                </div>
            </form>

            <div>
                <a
                    href="{{ route('profile.show') }}"
                    class="underline text-sm text-light-on-surface-variant hover:text-light-on-surface"
                >
                    {{ __('Редактировать профиль') }}
                </a>

                <form method="POST" action="{{ route('logout') }}" class="inline">
                    @csrf

                    <x-supports.button.filled type="submit">
                        {{ __('Выйти') }}
                    </x-supports.button.filled>
                </form>
            </div>
        </div>
    </x-supports.section.base>
</x-guest-layout>
