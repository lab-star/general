<x-guest-layout>
    <div class="mb-6">
        <a href="https://loyalgift.ru">
            <img src="{{ asset('images/lg-10.png') }}" alt="" class="w-36">
        </a>
    </div>

    <x-supports.section.base>
        <x-slot name="title">{{ __('Восстановление пароля') }}</x-slot>
        <x-slot name="subtitle">{{ __('Забыли пароль? Без проблем. Просто сообщите нам свой адрес электронной почты, и мы отправим вам ссылку для сброса пароля, которая позволит вам выбрать новый.') }}</x-slot>

        <form id="form-data" method="POST" action="{{ route('password.email') }}">
            @csrf

            <div>
                <x-supports.input.filled.text
                    name="email"
                    label="{{ __('Электронная почта') }}"
                    type="email"
                    :value="old('email')"
                    required
                    autofocus
                />
            </div>
        </form>

        <x-slot name="footer">
            <div></div>
            <x-supports.button.filled
                form="form-data"
                icon="paper-airplane"
                type="submit"
            >
                {{ __('Получить ссылку') }}
            </x-supports.button.filled>
        </x-slot>
    </x-supports.section.base>
</x-guest-layout>
