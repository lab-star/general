<x-guest-layout>

    <div class="mb-6">
        <a href="https://loyalgift.ru">
            <img src="{{ asset('images/lg-10.png') }}" alt="" class="w-36">
        </a>
    </div>

    <x-supports.section.base>

        <div class="flex flex-row mb-4">
            <a href="{{ route('login') }}" class="flex flex-row flex-auto justify-center px-6 py-2 sm:py-4 border-b-4 border-light-outline-variant hover:border-light-outline-variant dark:border-dark-outline-variant dark:hover:border-dark-outline-variant cursor-pointer transition">
                <x-icon::finger-print class="w-5 h-5" />
                <div class="ml-3 text-sm font-bold">{{ __('Войти') }}</div>
            </a>

            <a href="{{ route('register') }}" class="flex flex-row flex-auto justify-center px-6 py-2 sm:py-4 border-b-4 border-light-primary dark:border-dark-primary">
                <x-icon::heart class="w-5 h-5" />
                <div class="ml-3 text-sm font-bold">{{ __('Регистрация') }}</div>
            </a>
        </div>

        <form id="form-data" method="POST" action="{{ route('register') }}">
            @csrf

            <div  class="mt-6">
                <x-supports.input.filled.text
                    type="text"
                    name="name"
                    label="{{ __('Имя') }}"
                    :value="old('name')"
                    required
                    autofocu
                    autocomplete="name"
                />
            </div>

            <div class="mt-6">
                <x-supports.input.filled.text
                    type="email"
                    name="email"
                    label="{{ __('Электронная почта') }}"
                    :value="old('email')"
                    required
                />
            </div>

            <div class="mt-6">
                <x-supports.input.filled.text
                    type="password"
                    name="password"
                    label="{{ __('password') }}"
                    required
                    autocomplete="new-password"
                />
            </div>

            <div class="mt-6">
                <x-supports.input.filled.text
                    type="password"
                    name="password_confirmation"
                    label="{{ __('Повторите пароль') }}"
                    required
                    autocomplete="new-password"
                />
            </div>

{{--            <div class="flex flex-row items-center mt-6">--}}
{{--                <x-supports.input.checkbox--}}
{{--                    name="terms"--}}
{{--                    label="{{ __('Я соглашаюсь с условиями и бла-бла-бла') }}"--}}
{{--                    value="1"--}}
{{--                    required--}}
{{--                />--}}
{{--            </div>--}}
        </form>

        <x-slot name="footer">
            <div></div>
            <x-supports.button.filled
                form="form-data"
                type="submit"
                icon="heart"
            >
                {{ __('Продолжить') }}
            </x-supports.button.filled>
        </x-slot>

    </x-supports.section.base>

</x-guest-layout>
